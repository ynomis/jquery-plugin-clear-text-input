# [jQuery Plugin - Clear Text Input]

## Quick start
Inlcude 'jquery-plugin-cleartextinput.js' in your HTML page.

## Features

* Non-intrusive, only an add-on to input box alone.
* Cross-browser compatible (Chrome, Firefox, IE9+, Opera, Safari).

## Usage
```
    $('input').clearTextInput();
```
