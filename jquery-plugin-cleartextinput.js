/*!
 * jQuery plugin - clearTextInput, ver 0.1
 * @author Simon Yang, <simonxy@gmail.com>
 */
(function($) {
  $.fn.clearTextInput = function(options) {
    var opts = $.extend( {}, $.fn.clearTextInput.defaults, options );

    return this.each(function() {

      if ('input' !== this.nodeName.toLowerCase() || 
          'text' !== this.getAttribute('type')) {
        return;
      }

      var self = this;
      var wrapper = $('<span style="position:relative;">');
      var handler = $('<span style="cursor:pointer; position:absolute; text-decoration:none; color:' + 
                      opts.color + '; font-size:' + opts.size + '; right:' + opts.right + 
                      '; top:' + opts.top + '">' + opts.symbol + '</span>')
          .hover(function(event) {
            $(this).css( {'color' : opts.hoverColor} );
          }, function(event) {
            $(this).css( {'color' : opts.color} );
          }).click(function(event) {
            $(self).val('');
            $(self).focus();
            toggleClearAll();
            event.preventDefault();
            event.stopPropagation();
          });

      $(this).css({
        /**
         * Following CSS rules are preventing input box 
         * width getting affected by padding
         */
        'width':              $(this).width(),
        '-webkit-box-sizing': 'border-box',
        '-moz-box-sizing':    'border-box',
        'box-sizing':         'border-box'
      }).wrap(wrapper).after(handler);

      var toggleClearAll = function() {
        var _val    = $(self).val();
        var _focus  = $(self).is(':focus');
        if (!_focus || !_val) {
          handler.hide();
          $(self).css( {'padding-right' : 0} );
        } else {
          handler.show();
          $(self).css( {'padding-right' : opts.gutter} );
        }
      }

      toggleClearAll();

      $(this).bind('input propertychange', toggleClearAll)
      .bind('focus', function(event) {
        // blur event fires before the actual loss of focus,
        // (same for focus, it seemed.)
        // for checking the focus state, I need this trick
        setTimeout(toggleClearAll, 0);
      })
      .bind('blur', function(event) {
        setTimeout(function() {
          handler.hide('fast');
          $(self).css( {'padding-right' : 0} );
        }, 250);
      });
    });
  };

  $.fn.clearTextInput.defaults = {
    symbol:       '&otimes;',
    size:         '1.25em',
    gutter:       '1.3em',
    top:          '-0.15em',
    right:        '0.3em',
    color:        '#ccc',
    hoverColor:   '#999'
  };
})(jQuery);
